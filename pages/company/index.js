import { Fragment, useState } from 'react';
import { parseCookies } from "nookies";

// Components
import {
  InfoTextImage,
  Container,
  Brick,
  Grid,
  Persone,
  Banner,
  Modal,
  VacancyModal,
  Navigation
} from '../../components';

// Utils
import { SERVER_ENV_URL } from "../../utils/constants";

const Company = ({ data }) => {
  const [directionStash, setDirectionStash] = useState({});
  const [currentDirection, setCurrentDirection] = useState(null);
  const [activeFilter, setActiveFilter] = useState(data?.filters[0]?.id);

  const getDirection = async (id) => {
    let currDirectionData = directionStash[id];
    if (!currDirectionData) {
      const res = await fetch(`${SERVER_ENV_URL}/api/direction/${id}?Language=${data?.language}`);
      currDirectionData = await res.json();

      setDirectionStash({
        ...directionStash,
        [`${id}`]: currDirectionData,
      })
    }

    setCurrentDirection(currDirectionData);
  }

  return (
    <Fragment>
      <Navigation
        filtersOnCLick={(id) => setActiveFilter(id)}
        activeFilter={activeFilter}
        filtersList={data?.filters}
        filtersType="anchors"
      />

      <Container.Main>
        {data?.companyBlocks?.map((item, index) => (
          <Container.Block
            key={item.id}
            blockId={item.id}
            horizontalPadding
            verticalPadding={(index !== 0)}
            paddingBottom={(index === 0)}
          >
            <InfoTextImage
              blockIndex={index}
              reverse={item?.imagePositionReverse}
              title={item?.title}
              subTitle={item?.subtitle}
              text={item?.text}
              gallery={item?.images}
              attachments={item?.attachments}
            />
          </Container.Block>
        ))}

        {!!(data?.directions?.list?.length) && (
          <Container.Block
            verticalPadding
            blockId={data?.directions?.id}
            title={data?.directions?.title}
            subTitle={data?.directions?.subTitle}
            bgColor="#F9F9F9"
            maxWidth="1560px"
          >
            <Grid.Brick>
              {data?.directions?.list.map(item => (
                <Brick
                  key={item.id}
                  title={item?.name}
                  subTitle={item?.shortDescription}
                  onClick={() => getDirection(item.id)}
                  buttonText={data?.language === "Ru" ? "Подробнее" : "More"}
                />
              ))}
            </Grid.Brick>
          </Container.Block>
        )}

        {!!(data?.employee?.list?.length) && (
          <Container.Block
            verticalPadding
            horizontalPadding
            blockId={data?.employee?.id}
            title={data?.employee?.title}
            subTitle={data?.employee?.subTitle}
            maxWidth="1440px"
          >
            <Grid.Simple>
              {data?.employee?.list.map(item => (
                <Persone
                  key={item.id}
                  firstName={item?.firstName}
                  middleName={item?.middleName}
                  lastName={item?.lastName}
                  position={item?.position}
                  image={item?.photo?.id}
                />
              ))}
            </Grid.Simple>
          </Container.Block>
        )}

        {data?.banner && (
          <Container.Block
            horizontalPadding
            verticalPadding
          >
            <Banner
              type="vacancy"
              imgId={data?.banner?.backgroundImage?.id}
              title={data?.banner?.title}
              text={data?.banner?.text}
              urls={data?.banner?.urls}
              list={data?.banner?.list}
            />
          </Container.Block>
        )}
      </Container.Main>

      {(currentDirection !== null) && (
        <Modal onClick={() => setCurrentDirection(null)}>
          <VacancyModal
            title={currentDirection?.name}
            text={currentDirection?.fullDescription}
            vacancyTitle={data?.language === "Ru" ? "Вакансии" : "Vacancies"}
            vacancyList={currentDirection?.vacancies}
          />
        </Modal>
      )}
    </Fragment>
  );
}

export default Company;

Company.getInitialProps = async (ctx) => {
  const language = parseCookies(ctx).language || "Ru";

  const BLOCK_DEMANDED_VACANCIES = 'DemandedVacancies';
  const SEARCH_PARAMS = `?Language=Ru&SpecialBlockTypesFilter=${BLOCK_DEMANDED_VACANCIES}`;

  let resCompanyListData = null;
  let resDirectionListData = null;
  let resEmployeeListData = null;
  let resDemandedVacanciesData = null;
  let resDemandedListData = null;
  let filters = [];

  try {
    const resCompanyList = await fetch(`${SERVER_ENV_URL}/api/company/list?Language=${language}`);
    const resDirectionList = await fetch(`${SERVER_ENV_URL}/api/direction/list?Language=${language}`);
    const resEmployeeList = await fetch(`${SERVER_ENV_URL}/api/employee/list?Language=${language}`);

    const resSpecialBlockList = await fetch(`${SERVER_ENV_URL}/api/special-block/list${SEARCH_PARAMS}`);
    const resDemandedList = await fetch(`${SERVER_ENV_URL}/api/vacancy/list/demanded?Language=${language}`);

    resCompanyListData = await resCompanyList.json();
    resDirectionListData = await resDirectionList.json();
    resEmployeeListData = await resEmployeeList.json();

    const resSpecialBlockListData = await resSpecialBlockList.json();
    resDemandedListData = await resDemandedList.json();

    filters = resCompanyListData?.companyBlocks.map(item => ({
      id: item.id,
      name: item.title,
    }));

    resDemandedVacanciesData = resSpecialBlockListData?.projectTypes?.filter(
      item => item?.specialBlockType === BLOCK_DEMANDED_VACANCIES);
  } catch (err) {
    console.log('ABOUT PAGE ERROR ... ', err);
  }

  return {
    data: {
      language,
      filters: [...filters, {
        "id": "f22e00ff89b9139",
        "name": (language === "Ru" ? "Направления" : "Directions"),
      }, {
        "id": "f22e00ff89b9138",
        "name": (language === "Ru" ? "Команда" : "Our team"),
      }],
      companyBlocks: resCompanyListData?.companyBlocks,
      directions: {
        id: "f22e00ff89b9139",
        title: (language === "Ru" ? "Направления" : "Directions"),
        subTitle: (language === "Ru" ? "Широкий спектр направлений" : "A wide range of directions"),
        list: resDirectionListData?.directions,
      },
      employee: {
        id: "f22e00ff89b9138",
        title: (language === "Ru" ? "Команда" : "Team"),
        subTitle: (language === "Ru"
          ? "Творческая команда профессионалов высокого экспертного уровня"
          : "Creative team of high-level professionals"),
        list: resEmployeeListData?.employees,
      },
      banner: {
        ...resDemandedVacanciesData[0],
        list: resDemandedListData?.directions,
      }
    },
  };
}