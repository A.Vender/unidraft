// Components
import {
  Typography,
  Icon,
  Link,
} from '../../../../components';

// Styles
import styles from './styles.module.scss';

const ListItem = ({ type, value, linkText, linkUrl, linkType, linkTarget }) => {
  if (type === 'address') {
    return (
      <Typography.Text
        size="l"
        text={value}
      />
    );
  }

  return (
    <Link
      text={linkText}
      url={linkUrl}
      type={linkType}
      target={linkTarget}
    />
  );
};

const ContactsList = ({ list }) => {
  return (
    <ul className={styles.list}>
      {list?.map((item, index) => (
        <li key={index}>
          <div className={styles.list_info}>
            <Icon iconName={item.type} />
            <ListItem {...item} />
          </div>
        </li>
      ))}
    </ul>
  );
}

export default ContactsList;