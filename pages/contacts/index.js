import { Fragment } from 'react';
import { parseCookies } from "nookies";

// Components
import {
  Banner,
  Container,
  Typography,
  Form,
  Navigation
} from '../../components';
import ContactsList from './components/ContactsList';

// Utils
import { SERVER_ENV_URL } from "../../utils/constants";

// Styles
import styles from './styles.module.scss';

const Contacts = ({ data }) => {
  return (
    <Fragment>
      <Navigation />

      <Container.Main>
        <Container.Block
          horizontalPadding
          paddingBottom
        >
          <div className={styles.contacts}>
            <div className={styles.contacts_info}>
              <Typography.Title
                tag="h1"
                text={data?.title}
              />
              <Typography.Text
                size="l"
                text={data?.text}
              />

              {!!(data?.list?.length) && (
                <div className={styles.contacts_list}>
                  <ContactsList list={data?.list} />
                </div>
              )}
            </div>

            <div id="contact-form" className={styles.contacts_form}>
              <Form
                title={data?.language === "Ru" ? "Написать нам" : "Write to us"}
                isFile
              />
            </div>
          </div>
        </Container.Block>

        {!!(data?.bannerDirection?.list?.length) && (
          <Container.Block
            horizontalPadding
            verticalPadding
          >
            <Banner
              type="direction"
              imgId={data?.bannerDirection?.backgroundImage?.id}
              title={data?.bannerDirection?.title}
              text={data?.bannerDirection?.text}
              urls={data?.bannerDirection?.urls}
              list={data?.bannerDirection?.list}
            />
          </Container.Block>
        )}

        {!!(data?.bannerVacancies?.list?.length) && (
          <Container.Block
            horizontalPadding
            verticalPadding
          >
            <Banner
              type="vacancy"
              imgId={data?.bannerVacancies?.backgroundImage?.id}
              title={data?.bannerVacancies?.title}
              text={data?.bannerVacancies?.text}
              urls={data?.bannerVacancies?.urls}
              list={data?.bannerVacancies?.list}
            />
          </Container.Block>
        )}
      </Container.Main>
    </Fragment>
  );
}

export default Contacts;

Contacts.getInitialProps = async (ctx) => {
  const language = parseCookies(ctx).language || "Ru";

  const BLOCK_DEMANDED_DIRECTIONS = 'DemandedDirections';
  const BLOCK_DEMANDED_VACANCIES = 'DemandedVacancies';
  const SEARCH_PARAMS = `?Language=${language}&SpecialBlockTypesFilter=${BLOCK_DEMANDED_DIRECTIONS}&SpecialBlockTypesFilter=${BLOCK_DEMANDED_VACANCIES}`;

  let resContactsData = null;
  let resDemandedDirectionListData = null;
  let resDemandedVacancyListData = null;

  let demandedDirections = null;
  let demandedVacancies = null;

  try {
    const resContacts = await fetch(`${SERVER_ENV_URL}/api/contact/page?Language=${language}`);
    resContactsData = await resContacts.json();

    const resSpecialBlockList = await fetch(`${SERVER_ENV_URL}/api/special-block/list${SEARCH_PARAMS}`);
    const resDemandedVacancyList = await fetch(`${SERVER_ENV_URL}/api/vacancy/list/demanded?Language=${language}`);
    const resDemandedDirectionList = await fetch(`${SERVER_ENV_URL}/api/direction/list/demanded?Language=${language}`);

    const resSpecialBlockListData = await resSpecialBlockList.json();
    resDemandedVacancyListData = await resDemandedVacancyList.json();
    resDemandedDirectionListData = await resDemandedDirectionList.json();

    demandedDirections = resSpecialBlockListData?.projectTypes?.filter(
      item => item?.specialBlockType === BLOCK_DEMANDED_DIRECTIONS);

    demandedVacancies = resSpecialBlockListData?.projectTypes?.filter(
      item => item?.specialBlockType === BLOCK_DEMANDED_VACANCIES);
  } catch (err) {
    console.log('ABOUT PAGE ERROR ... ', err);
  }

  return {
    data: {
      language,
      title: (language === "Ru" ? "Контакты" : "Contacts"),
      text: resContactsData?.pageText,
      list: [{
        type: "tel",
        value: resContactsData?.phoneNumber,
        linkUrl: `tel:${resContactsData?.phoneNumber}`,
        linkText: resContactsData?.phoneNumber,
        linkType: null,
        linkTarget : null
      }, {
        type: "mailto",
        value: resContactsData?.email,
        linkUrl: `mailto:${resContactsData?.email}`,
        linkText: resContactsData?.email,
        linkType: null,
        linkTarget : null
      }, {
        type: "address",
        value: resContactsData?.address,
        linkText: resContactsData?.address,
        linkUrl: null,
        linkType: null,
        linkTarget : null
      }, {
        type: "map",
        value: resContactsData?.mapLinks?.url,
        linkUrl: resContactsData?.mapLinks?.url,
        linkText: (language === "Ru" ? "Посмотреть на Google maps" : "View on Google maps"),
        linkType: "underline",
        linkTarget : "_blank"
      }],
      bannerDirection: {
        ...demandedDirections[0],
        list: resDemandedDirectionListData?.directions,
      },
      bannerVacancies: {
        ...demandedVacancies[0],
        list: resDemandedVacancyListData?.directions,
      }
    }
  }
}