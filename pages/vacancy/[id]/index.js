import { useState, Fragment } from 'react';
import { parseCookies } from "nookies";

// Components
import {
  Banner,
  Container,
  Link,
  Typography,
  Button,
  Navigation,
  Modal,
  Form,
} from '../../../components';

// Utils
import { SERVER_ENV_URL } from "../../../utils/constants";

// Styles
import styles from './styles.module.scss';

const VacancyPage = ({ data }) => {
  const [isModalOpen, setOpenModal] = useState(false);
  return (
    <Fragment>
      <Navigation />

      <Container.Main>
        <div className={styles.vacancypage}>
          <div className={styles.vacancypage_link}>
            <Link
              url="/vacancy"
              text={data?.language === "Ru" ? "Все вакансии" : "All vacancies"}
              iconName="arrow-left-gray"
              color="gray"
            />
          </div>

          <Typography.Title
            tag="h1"
            text={data?.title}
          />

          <div
            className={styles.vacancypage_text}
            dangerouslySetInnerHTML={{ __html: data?.description }}
          />

          <div className={styles.vacancypage_button}>
            <Button
              onClick={() => setOpenModal(!isModalOpen)}
              text={data?.language === "Ru" ? "Откликнуться" : "Respond"}
            />
          </div>
        </div>

        {data?.banner && (
          <Container.Block
            horizontalPadding
            verticalPadding
          >
            <Banner
              type="direction"
              imgId={data?.banner?.backgroundImage?.id}
              title={data?.banner?.title}
              text={data?.banner?.text}
              urls={data?.banner?.urls}
              list={data?.banner?.list}
            />
          </Container.Block>
        )}
      </Container.Main>

      {isModalOpen && (
        <Modal onClick={() => setOpenModal(!isModalOpen)}>
          <div className={styles.vacancypage_form}>
            <Form
              title={data?.language === "Ru" ? "Отклик на вакансию" : "Response to a job"}
              isFile
            />
          </div>
        </Modal>
      )}
    </Fragment>
  );
}

export default VacancyPage;

VacancyPage.getInitialProps = async (ctx) => {
  const language = parseCookies(ctx).language || "Ru";

  const BLOCK_DEMANDED_DIRECTIONS = 'DemandedDirections';
  const SEARCH_PARAMS = `?Language=${language}&SpecialBlockTypesFilter=${BLOCK_DEMANDED_DIRECTIONS}`;
  const vacancyId = ctx?.req?.url.replace('/vacancy/', '');

  let resVacanciesData = null;
  let resDemandedDirectionsData = null;
  let resDemandedListData = null;

  try {
    const resVacancies = await fetch(`${SERVER_ENV_URL}/api/vacancy/${vacancyId}/?Language=${language}`);
    resVacanciesData = await resVacancies.json();

    const resSpecialBlockList = await fetch(`${SERVER_ENV_URL}/api/special-block/list${SEARCH_PARAMS}`);
    const resDemandedList = await fetch(`${SERVER_ENV_URL}/api/direction/list/demanded?Language=${language}`);

    const resSpecialBlockListData = await resSpecialBlockList.json();
    resDemandedListData = await resDemandedList.json();

    resDemandedDirectionsData = resSpecialBlockListData?.projectTypes?.filter(
      item => item?.specialBlockType === BLOCK_DEMANDED_DIRECTIONS);

  } catch (err) {
    console.log('ABOUT PAGE ERROR ... ', err);
  }

  return {
    data: {
      language,
      ...resVacanciesData,
      banner: {
        ...resDemandedDirectionsData[0],
        list: resDemandedListData?.directions,
      }
    }
  }
}