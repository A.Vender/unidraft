import { useState, Fragment } from 'react';
import { parseCookies } from "nookies";

// Components
import {
  Banner,
  Brick,
  Container,
  Grid,
  Modal,
  VacancyModal,
  Navigation
} from '../../components';

// Utils
import { SERVER_ENV_URL } from "../../utils/constants";

// Styles
import styles from './styles.module.scss';

const Vacancy = ({ data }) => {
  const [activeFilter, setActiveFilter] = useState("all");
  const [directionsList, setDirectionsList] = useState(data?.directions);
  const [directionStash, setDirectionStash] = useState({});
  const [currentDirection, setCurrentDirection] = useState(null);

  const filterProjects = (id) => {
    const filterList = id === 'all'
        ? data?.directions
        : data?.directions.filter(item => item.id === id);

    setDirectionsList(filterList);
    setActiveFilter(id);
  }

  const getDirection = async (e, id) => {
    if (e.target.localName !== "a") {
      let currDirectionData = directionStash[id];
      if (!currDirectionData) {
        const res = await fetch(`${SERVER_ENV_URL}/api/direction/${id}?Language=${data?.language}`);
        currDirectionData = await res.json();

        setDirectionStash({
          ...directionStash,
          [`${id}`]: currDirectionData,
        })
      }

      setCurrentDirection(currDirectionData);
    }
  }

  return (
    <Fragment>
      <Navigation
        filtersOnCLick={filterProjects}
        activeFilter={activeFilter}
        filtersList={data?.filters}
        filtersType="filters"
      />

      <Container.Main>
        <div className={styles.vacancy}>
          {!!(directionsList?.length) && (
            <Container.Block
              paddingBottom
              bgColor="#F9F9F9"
              maxWidth="1560px"
            >
              <div className={styles.vacancy_bricks}>
                <Grid.Brick>
                  {directionsList?.map((item, index) => (
                    <Brick
                      key={index}
                      buttonText={data?.language === "Ru" ? "Подробнее" : "More"}
                      title={item?.name}
                      list={item?.vacancies}
                      onClick={(e) => getDirection(e, item.id)}
                    />
                  ))}
                </Grid.Brick>
              </div>
            </Container.Block>
          )}

          {data?.banner && (
            <Container.Block
              horizontalPadding
              verticalPadding
            >
              <Banner
                type="direction"
                imgId={data?.banner?.backgroundImage?.id}
                title={data?.banner?.title}
                text={data?.banner?.text}
                urls={data?.banner?.urls}
                list={data?.banner?.list}
              />
            </Container.Block>
          )}

          {(currentDirection !== null) && (
            <Modal onClick={() => setCurrentDirection(null)}>
              <VacancyModal
                title={currentDirection?.name}
                text={currentDirection?.fullDescription}
                vacancyTitle={data?.language === "Ru" ? "Вакансии" : "Vacancy"}
                vacancyList={currentDirection?.vacancies}
              />
            </Modal>
          )}
        </div>
      </Container.Main>
    </Fragment>
  );
}

export default Vacancy;


Vacancy.getInitialProps = async (ctx) => {
  const language = parseCookies(ctx).language || "Ru";

  const BLOCK_DEMANDED_DIRECTIONS = 'DemandedDirections';
  const SEARCH_PARAMS = `?Language=${language}&SpecialBlockTypesFilter=${BLOCK_DEMANDED_DIRECTIONS}`;

  let resWithVacanciesData = null;
  let resDemandedDirectionsData = null;
  let resDemandedListData = null;
  let filters = [];

  try {
    const resWithVacancies = await fetch(`${SERVER_ENV_URL}/api/direction/list/with-vacancies?Language=${language}`);
    resWithVacanciesData = await resWithVacancies.json();

    const resSpecialBlockList = await fetch(`${SERVER_ENV_URL}/api/special-block/list${SEARCH_PARAMS}`);
    const resDemandedList = await fetch(`${SERVER_ENV_URL}/api/direction/list/demanded?Language=${language}`);

    const resSpecialBlockListData = await resSpecialBlockList.json();
    resDemandedListData = await resDemandedList.json();

    resDemandedDirectionsData = resSpecialBlockListData?.projectTypes?.filter(
      item => item?.specialBlockType === BLOCK_DEMANDED_DIRECTIONS);

    filters = resWithVacanciesData?.directions?.map(item => ({
      id: item.id,
      name: item.shortName,
    }));
  } catch (err) {
    console.log('ABOUT PAGE ERROR ... ', err);
  }

  return {
    data: {
      language,
      filters: [{
        id: "all",
        name: (language === "Ru" ? "Все направления" : "All directions")
      }, ...filters],
      directions: resWithVacanciesData?.directions,
      banner: {
        ...resDemandedDirectionsData[0],
        list: resDemandedListData?.directions,
      }
    }
  };
}