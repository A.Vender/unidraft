import { Fragment, useEffect, useState } from 'react';
import { parseCookies } from 'nookies';

// Components
import {
  Footer,
  Container,
  Greeting,
} from '../components';

// Utils
import { SERVER_ENV_URL } from "../utils/constants";

// Styles
import '../styles/globals.scss';
import '../styles/fonts.scss';

function MyApp({ Component, pageProps }) {
  const [isGreeting, setIsGreeting] = useState(false);
  const [footerData, setFooterData] = useState({});
  const language = parseCookies()?.language || "Ru";

  // Кешируем запрос к футеру
  const getFooterData = async () => {
    if (sessionStorage) {
      const isHasInStorage = sessionStorage.getItem('footer');
      if (isHasInStorage) {
        setFooterData(JSON.parse(isHasInStorage));
      } else {
        const res = await fetch(`${SERVER_ENV_URL}/api/contact/footer?Language=${language}`);
        const resData = await res.json();
        setFooterData(resData);
        sessionStorage.setItem('footer', JSON.stringify(resData));
      }
    }
  }

  useEffect(() => {
    if (document !== undefined) {
      const isHasProject = document.cookie.match(/project=UNIDRAFT/g);
      setIsGreeting(!isHasProject);
    }

    getFooterData();
  }, []);

  return (
    <Fragment>
      {isGreeting && (
        <Greeting
          text="unidraft"
          callback={() => setIsGreeting(false)}
        />
      )}

      <Component {...pageProps} />

      <Container.Block
        horizontalPadding
        verticalPadding
      >
        <Footer {...footerData} language={language} />
      </Container.Block>
    </Fragment>
  );
}

export default MyApp;
