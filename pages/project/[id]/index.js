import { useState, Fragment } from 'react';
import { parseCookies } from "nookies";

// Components
import {
  Typography,
  ParallaxContainer,
  Container,
  Grid,
  ProjectCard,
  Modal,
  ModalProjectCard,
  Banner,
  Navigation
} from '../../../components';
import Gallery from "./components/Gallery";

// Styles
import styles from './styles.module.scss';

// Utils
import { SERVER_ENV_URL } from "../../../utils/constants";

const Project = ({ data }) => {
  const [projectsStash, setProjectsStash] = useState({});
  const [currentProject, setCurrentProject] = useState(null);
  const [flippingProjectIndex, setFlippingProjectIndex] = useState(null);

  const getProject = async (id) => {
    let projectData = projectsStash[id];

    if (!projectData) {
      const res = await fetch(`${SERVER_ENV_URL}/api/project/${id}?Language=${data?.language}`);
      projectData = await res.json();

      setProjectsStash({
        ...projectsStash,
        [`${id}`]: projectData,
      });
    }

    setCurrentProject(projectData);
  }

  const flippingProject = (index) => {
    const projectId = data?.projects[index]?.id;
    setFlippingProjectIndex(index);
    getProject(projectId);
  }

  return (
    <Fragment>
      <Navigation />

      <Container.Main>
        <div className={styles.project}>
          <div className={styles.project_heading}>
            <div className={styles.project_heading__subtitle}>
              {data?.projectTypes.map(item => data?.projectTypeIds?.includes(item.id) ? (
                <Typography.SubTitle
                  text={item?.name}
                  size="l"
                  color="gray"
                />
              ) : null)}
            </div>
            <Typography.Title
              text={data?.name}
              tag="h1"
            />
          </div>

          <ul className={styles.project_info}>
            {data?.projectInfo?.map((item, index) => (
              <li key={index}>
                <Typography.SubTitle
                  className={styles.project_info__name}
                  text={item.name}
                  color="gray"
                  size="m"
                />
                <Typography.Text
                  text={`${item.value} ${item.unit || ''}`}
                  size="xxl"
                />
              </li>
            ))}
          </ul>

          {data?.mainImage && (
            <Container.Block
              verticalPadding
              maxWidth="100%"
            >
              <ParallaxContainer
                image={data?.mainImage?.id}
              />
            </Container.Block>
          )}

          {data?.description && (
            <Container.Block horizontalPadding>
              <Typography.Title
                text={data?.language === "Ru" ? "О проекте" : "About the project"}
                tag="h2"
              />
              <div
                className={styles.project_about}
                dangerouslySetInnerHTML={{ __html: data?.description }}
              />
            </Container.Block>
          )}

          {!!(data?.galleryImageLength) && (
            <Container.Block
              horizontalPadding
              verticalPadding
            >
              <Gallery
                gallery={data?.images}
                galleryLength={data?.galleryImageLength}
              />
            </Container.Block>
          )}

          {(!!data?.projects?.length) && (
            <Container.Block
              verticalPadding
              title={data?.language === "Ru" ? "Еще проекты" : "More projects"}
              maxWidth="1560px"
            >
              <Grid.Brick>
                {data?.projects?.map((item) => (
                  <ProjectCard
                    onClick={() => getProject(item.id)}
                    key={item.id}
                    projectTypeList={data?.typeList}
                    {...item}
                  />
                ))}
              </Grid.Brick>

              {(currentProject !== null) && (
                <Modal onClick={() => setCurrentProject(null)}>
                  <ModalProjectCard
                    language={data?.language}
                    callback={flippingProject}
                    projectLength={data?.projects?.length}
                    projectIndex={flippingProjectIndex}
                    {...currentProject}
                  />
                </Modal>
              )}
            </Container.Block>
          )}

          {data?.banner && (
            <Container.Block
              horizontalPadding
              verticalPadding
            >
              <Banner
                type="vacancy"
                imgId={data?.banner?.backgroundImage?.id}
                title={data?.banner?.title}
                text={data?.banner?.text}
                urls={data?.banner?.urls}
                list={data?.banner?.list}
              />
            </Container.Block>
          )}
        </div>
      </Container.Main>
    </Fragment>
  );
};

export default Project;

Project.getInitialProps = async (ctx) => {
  const language = parseCookies(ctx).language || "Ru";
  const projectId = ctx?.req?.url.replace('/project/', '');
  const BLOCK_DEMANDED_VACANCIES = 'DemandedVacancies';
  const SEARCH_PARAMS = `?Language=${language}&SpecialBlockTypesFilter=${BLOCK_DEMANDED_VACANCIES}`;

  let resProjectData = null;
  let resProjectTypeListData = null;
  let resProjectListData = null;

  let resSpecialBlockListData = null;
  let resDemandedListData = null;
  let demandedVacancies = null;

  try {
    const resProjectList = await fetch(`${SERVER_ENV_URL}/api/project/list?Language=${language}`);
    const resProject = await fetch(`${SERVER_ENV_URL}/api/project/${projectId}?Language=${language}`);
    const resProjectTypeList = await fetch(`${SERVER_ENV_URL}/api/project/type/list?Language=${language}`);

    const resSpecialBlockList = await fetch(`${SERVER_ENV_URL}/api/special-block/list${SEARCH_PARAMS}`);
    const resDemandedList = await fetch(`${SERVER_ENV_URL}/api/vacancy/list/demanded?Language=${language}`);

    resProjectData = await resProject.json();
    resProjectTypeListData = await resProjectTypeList.json();
    resProjectListData = await resProjectList.json();

    resSpecialBlockListData = await resSpecialBlockList.json();
    resDemandedListData = await resDemandedList.json();

    demandedVacancies = resSpecialBlockListData?.projectTypes?.filter(
      item => item?.specialBlockType === BLOCK_DEMANDED_VACANCIES);
  } catch (err) {
    console.log('ABOUT PAGE ERROR ... ', err);
  }

  return {
    data: {
      language,
      galleryImageLength: resProjectData?.images?.length,
      projectTypes: resProjectTypeListData?.projectTypes,
      ...resProjectData,
      projects: resProjectListData?.projects,
      projectInfo: [{
        type: "spaceArea",
        name: (language === "Ru" ? "Общая площадь" : "Total area"),
        value: resProjectData?.spaceArea,
        unit: (language === "Ru" ? "м²" : "m²")
      }, {
        type: "constructionYear",
        name: (language === "Ru" ? "Год реализации" : "Year of implementation"),
        value: resProjectData?.constructionYear,
        unit: null
      }, {
        type: "location",
        name: (language === "Ru" ? "Расположение" : "Location"),
        value: resProjectData?.location,
        unit: null
      }],
      banner: {
        ...demandedVacancies[0],
        list: resDemandedListData?.directions,
      }
    }
  }
};
