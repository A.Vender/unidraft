import LazyLoad from 'react-lazyload';

// Styles
import styles from './styles.module.scss';

// Utils
import { STATIC_FILE_URL } from "../../../../../utils/constants";

const GalleryImage = ({ imageId }) => (
	<LazyLoad>
		<img
			alt={`image_${imageId}`}
			className={styles.gallery_imageBg}
			src={`${STATIC_FILE_URL}/${imageId}`}
		/>
	</LazyLoad>
);

const Gallery = ({ gallery, galleryLength }) => {
	if (!gallery?.length) return null;

	if (galleryLength === 2) {
		return (
			<ul className={styles.gallery_simple}>
				{gallery?.map(item => (
					<li className key={item.id}>
						<GalleryImage imageId={item.id} />
					</li>
				))}
			</ul>
		);
	}

	if (galleryLength === 3) {
		return (
			<div className={styles.gallery}>
				<div className={styles.gallery_bigImage}>
					<GalleryImage imageId={gallery[0]?.id} />
				</div>
				<ul className={styles.gallery_doubleImage}>
					{gallery.slice(1)?.map(item => (
						<li key={item.id}>
							<GalleryImage imageId={item.id} />
						</li>
					))}
				</ul>
			</div>
		);
	}

	if (galleryLength === 4) {
		return (
			<div className={styles.gallery}>
				<div className={styles.gallery_bigImage}>
					<GalleryImage imageId={gallery[0]?.id} />
				</div>
				<ul className={styles.gallery_doubleImage}>
					{gallery.slice(1, 3)?.map(item => (
						<li key={item.id}>
							<GalleryImage imageId={item.id} />
						</li>
					))}
				</ul>
				<div className={styles.gallery_bigImage}>
					<GalleryImage imageId={gallery[3]?.id} />
				</div>
			</div>
		);
	}

	if (galleryLength === 5) {
		return (
			<div className={styles.gallery}>
				<div className={styles.gallery_bigImage}>
					<GalleryImage imageId={gallery[0]?.id} />
				</div>
				<ul className={styles.gallery_doubleImage}>
					{gallery.slice(1, 3)?.map(item => (
						<li key={item.id}>
							<GalleryImage imageId={item.id} />
						</li>
					))}
				</ul>
				<div className={styles.gallery_bigImage}>
					<GalleryImage imageId={gallery[3]?.id} />
				</div>
				<div className={styles.gallery_bigImage}>
					<GalleryImage imageId={gallery[3]?.id} />
				</div>
			</div>
		);
	}

	if (galleryLength === 6) {
		return (
			<div className={styles.gallery}>
				<div className={styles.gallery_bigImage}>
					<GalleryImage imageId={gallery[0]?.id} />
				</div>
				<ul className={styles.gallery_doubleImage}>
					{gallery.slice(1, 3)?.map(item => (
						<li key={item.id}>
							<GalleryImage imageId={item.id} />
						</li>
					))}
				</ul>
				<div className={styles.gallery_bigImage}>
					<GalleryImage imageId={gallery[3]?.id} />
				</div>
				<ul className={styles.gallery_doubleImage}>
					{gallery.slice(3, 5)?.map(item => (
						<li key={item.id}>
							<GalleryImage imageId={item.id} />
						</li>
					))}
				</ul>
			</div>
		);
	}

	return (
		<div className={styles.gallery}>
			<div className={styles.gallery_bigImage}>
			</div>
			<ul className={styles.gallery_doubleImage}>
				{gallery.slice(1, 3)?.map(item => (
					<li key={item.id}>
						<GalleryImage imageId={item.id} />
					</li>
				))}
			</ul>
			<div className={styles.gallery_bigImage}>
				<GalleryImage imageId={gallery[3].id} />
			</div>
			<ul className={styles.gallery_doubleImage}>
				{gallery.slice(3, 5)?.map(item => (
					<li key={item.id}>
						<GalleryImage imageId={item.id} />
					</li>
				))}
			</ul>
			<ul className={styles.gallery_list}>
				{gallery.slice(5)?.map(item => (
					<li key={item.id}>
						<GalleryImage imageId={item.id} />
					</li>
				))}
			</ul>
		</div>
	);
}

export default Gallery;