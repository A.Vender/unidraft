// Components
import  {
  Typography,
  Button,
  Link,
  Banner,
  ImagesSet,
  Brick,
  Navigation,
  Footer,
} from '../../components';

// Styles
import styles from './styles.module.scss';

const unsplash = 'https://images.unsplash.com/photo-1521737711867-e3b97375f902?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=987&q=80';
const unsplash1 = 'https://images.unsplash.com/photo-1497215728101-856f4ea42174?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80';
const unsplash2 = 'https://images.unsplash.com/photo-1535957998253-26ae1ef29506?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1036&q=80';
const unsplash3 = 'https://images.unsplash.com/photo-1497032628192-86f99bcd76bc?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80';

const CompContainer = ({ children }) => (
  <ul className={styles.list}>
    {children.map((item, index) => (
      <li key={index}>
        {item}
      </li>
    ))}
  </ul>
);

const Box = ({ title, children }) => (
  <div className={styles.item}>
    <div className={styles.item_title}>{title}</div>
    {children}
  </div>
);

const Components = () => {
  return (
    <div className={styles.components}>
      <CompContainer>
        {/* ----------- Типографика ----------- */}
        <Box title="Заголовок h1, h2, h3">
          <Typography.Title
            tag="h1"
            text="Микрорайон «Домашний»"
          />
          <Typography.Title
            tag="h2"
            text="Микрорайон «Домашний»"
          />
          <Typography.Title
            tag="h3"
            text="Микрорайон «Домашний»"
          />
        </Box>
        <Box title="Подзаголовки">
          <Typography.SubTitle
            size="xl"
            text="Микрорайон «Домашний»"
          />
          <Typography.SubTitle
            size="l"
            text="Микрорайон «Домашний»"
          />
          <Typography.SubTitle
            size="m"
            text="Микрорайон «Домашний»"
          />
          <Typography.SubTitle
            size="s"
            text="Микрорайон «Домашний»"
          />
        </Box>
        <Box title="Текст">
          <Typography.Text
            size="xl"
            text="Микрорайон «Домашний»"
          />
          <Typography.Text
            size="l"
            text="Микрорайон «Домашний»"
          />
          <Typography.Text
            size="m"
            text="Микрорайон «Домашний»"
          />
          <Typography.Text
            size="s"
            text="Микрорайон «Домашний»"
          />
        </Box>

        {/* ----------- Кнопки ----------- */}
        <Box title="Обычная кнопка">
          <Button
            text="Присоедениться к нам"
          />
        </Box>
        <Box title="Кнопка с иконкой">
          <Button
            text="Откликнуться"
            iconName="file"
          />
        </Box>

        {/* ----------- Ссылки ----------- */}
        <Box title="Обычная ссылка">
          <Link
            url="/"
            text="Ведущий инженер-проектировщик СС"
          />
        </Box>
        <Box title="Ссылка с подчеркиванием">
          <Link
            url="/"
            type="underline"
            text="Ведущий инженер-проектировщик СС"
          />
        </Box>
        <Box title="Ссылка с background">
          <Link
            url="/"
            color="white"
            type="background"
            text="Присоедениться к нам"
          />
        </Box>
        <Box title="Ссылка с background на весь экран">
          <Link
            url="/"
            width="100%"
            color="white"
            type="background"
            text="Присоедениться к нам"
          />
        </Box>
        <Box title="Ссылка с background и icon на весь экран">
          <Link
            url="/"
            iconName="file"
            width="100%"
            color="white"
            type="background"
            text="Присоедениться к нам"
          />
        </Box>
        <Box title="Ссылка с background и icon на весь экран">
          <Link
            url="/"
            iconName="map-pin"
            type="underline"
            text="Присоедениться к нам"
          />
        </Box>


        {/* ----------- Баннер ----------- */}
        <Box title="Баннер">
          <Banner
            imgId={unsplash}
            title="Вакансии"
            subTitle="Присоединяйтесь к нашей команде!"
            buttonText="Подробнее"
            buttonUrl="/"
            list={[{
              "name": "BIM-координатор",
              "url": "/"
            }, {
              "name": "Ведущий Архитектор / Архитектор",
              "url": "/"
            }, {
              "name": "Главный / Ведущий / Инженер-конструктор",
              "url": "/"
            }, {
              "name": "Главный специалист / Инженер ОВиК",
              "url": "/"
            }, {
              "name": "Главный специалист / Инженер ВК",
              "url": "/"
            }, {
              "name": "Главный специалист / Инженер ПТ",
              "url": "/"
            }]}
          />
        </Box>

        {/* ----------- Сет из картинок ----------- */}
        <Box title="Сет из картинок">
          <ImagesSet images={[
            unsplash1,
            unsplash2,
            unsplash3
          ]} />
        </Box>

        {/* ----------- Плитки для вакансий и направлений ----------- */}
        <Box title="Плитка со списком">
          <Brick
            title="Отопление, вентиляция и кондиционирование"
            subtitle="Вакансии"
            list={[{
              "name": "Ведущий  инженер-проектировщик ОВиК",
              "url": "/"
            }, {
              "name": "Главный специалист  ОВиК",
              "url": "/"
            }, {
              "name": "Инженер-проектировщик ОВиК",
              "url": "/"
            }]}
          />
        </Box>
        <Box title="Плитка с текстом">
          <Brick
            title="Отопление, вентиляция и кондиционирование"
            text="Разработали собственное программное обеспечение, чтобы снизить трудозатраты. Используем BiM моделирование, чтобы работать эффективно."
            onClick={() => null}
            buttonText="Подробнее"
          />
        </Box>
      </CompContainer>
    </div>
  );
};

export default Components;
