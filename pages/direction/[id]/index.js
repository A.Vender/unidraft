import { useState, Fragment } from 'react';
import { parseCookies } from "nookies";

// Components
import {
  Banner,
  Container,
  Link,
  Typography,
  Button,
  Navigation,
  Modal,
  Form,
} from '../../../components';

// Utils
import { SERVER_ENV_URL } from "../../../utils/constants";

// Styles
import styles from './styles.module.scss';

const DirectionPage = ({ data }) => {
  const [isModalOpen, setOpenModal] = useState(false);

  return (
    <Fragment>
      <Navigation />

      <Container.Main>
        <div className={styles.directionpage}>
          <div className={styles.directionpage_link}>
            <Link
              url="/vacancy"
              text={data?.language === "Ru" ? "Все вакансии" : "All vacancies"}
              iconName="arrow-left-gray"
              color="gray"
            />
          </div>

          <Typography.Title
            tag="h1"
            text={data?.name}
          />

          <div
            className={styles.directionpage_text}
            dangerouslySetInnerHTML={{ __html: data?.fullDescription }}
          />
          {!!(data?.vacancies?.length) && (
            <div className={styles.directionpage_vacancy}>
              <Typography.Title
                tag="h3"
                text={data?.language === "Ru" ? "Вакансии" : "Vacancies"}
              />
              <ul>
                {data?.vacancies.map(item => (
                  <li key={item.id}>
                    <Link
                      size="l"
                      url={`/vacancy/${item.id}`}
                      text={item.title}
                      type="underline"
                    />
                  </li>
                ))}
              </ul>
            </div>
          )}
        </div>

        {data?.banner && (
          <Container.Block
            horizontalPadding
            verticalPadding
          >
            <Banner
              type="vacancy"
              imgId={data?.banner?.backgroundImage?.id}
              title={data?.banner?.title}
              text={data?.banner?.text}
              urls={data?.banner?.urls}
              list={data?.banner?.list}
            />
          </Container.Block>
        )}
      </Container.Main>

      {isModalOpen && (
        <Modal onClick={() => setOpenModal(!isModalOpen)}>
          <div className={styles.directionpage_form}>
            <Form
              title={data?.language === "Ru" ? "Отклик на вакансию" : "Response to a job"}
              isFile
            />
          </div>
        </Modal>
      )}
    </Fragment>
  );
}

export default DirectionPage;

DirectionPage.getInitialProps = async (ctx) => {
  const language = parseCookies(ctx).language || "Ru";

  const BLOCK_DEMANDED_VACANCIES = 'DemandedVacancies';
  const SEARCH_PARAMS = `?Language=${language}&SpecialBlockTypesFilter=${BLOCK_DEMANDED_VACANCIES}`;
  const directionId = ctx?.req?.url.replace('/direction/', '');

  let resDirectionData = null;
  let resDemandedVacanciesData = null;
  let resVacanciesListData = null;

  try {
    const resDirection = await fetch(`${SERVER_ENV_URL}/api/direction/${directionId}/?Language=${language}`);
    resDirectionData = await resDirection.json();

    const resSpecialBlockList = await fetch(`${SERVER_ENV_URL}/api/special-block/list${SEARCH_PARAMS}`);
    const resDemandedList = await fetch(`${SERVER_ENV_URL}/api/direction/list/demanded?Language=${language}`);

    const resSpecialBlockListData = await resSpecialBlockList.json();
    resVacanciesListData = await resDemandedList.json();

    resDemandedVacanciesData = resSpecialBlockListData?.projectTypes?.filter(
      item => item?.specialBlockType === BLOCK_DEMANDED_VACANCIES);

  } catch (err) {
    console.log('ABOUT PAGE ERROR ... ', err);
  }

  return {
    data: {
      language,
      ...resDirectionData,
      banner: {
        ...resDemandedVacanciesData[0],
        list: resVacanciesListData?.directions,
      }
    }
  }
}