module.exports = {
  reactStrictMode: true,
  experimental: {
    outputStandalone: true,
  },
  images: {
    loader: 'imgix',
    path: 'http://85.193.86.104/backend/api/file',
    domains: ['85.193.86.104'],
  },
};
