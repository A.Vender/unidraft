const dev = process.env.NODE_ENV !== 'production';
export const SERVER_ENV_URL = dev ? 'http://localhost:3000' : 'http://85.193.86.104/backend';

export const STATIC_FILE_URL = 'http://85.193.86.104/backend/api/file';

export const SIZE_FAKE_BRICKS = ['70%', '100%', "60%", '60%', '40%', '80%', '40%', '60%', '100%', '100%', "60%", '60%', '40%'];

export const EMAIL_REG_EXP = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
export const PHONE_NUMBER_REG_EXP = /[^\d]/g;