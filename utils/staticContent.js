export const MENU = {
	Ru: [{
		alias: "projects",
		name: 'Проекты',
		url: '/'
	}, {
		alias: "company",
		name: 'Компания',
		url: '/company'
	}, {
		alias: "vacancy",
		name: 'Вакансии',
		url: '/vacancy'
	}, {
		alias: "contacts",
		name: 'Контакты',
		url: '/contacts'
	}],
	Eng: [{
		alias: "projects",
		name: 'Projects',
		url: '/'
	}, {
		alias: "company",
		name: 'Company',
		url: '/company'
	}, {
		alias: "vacancy",
		name: 'Vacancies',
		url: '/vacancy'
	}, {
		alias: "contacts",
		name: 'Contacts',
		url: '/contacts'
	}]
}

export const FORM_ERROR_OBJECT = {
	Ru: {
		fullName: "Введите полное имя",
		email: "Введите корректный email",
		phoneNumber: "Введите корректный телефон",
		message: "Введите сщщбщение",
	},
	Eng: {
		fullName: "Enter full name",
		email: "Please enter a valid email",
		phoneNumber: "Please enter a valid phone",
		message: "Enter message",
	}
};