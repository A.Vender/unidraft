const express = require('express');
const router = express.Router();

/** Projects Api */
router.get("/project/list/", (req, res) => {
	return res.send(JSON.stringify(require("../data/project/list.json")));
});
router.get("/project/type/list/", (req, res) => {
	return res.send(JSON.stringify(require("../data/project/typeList.json")));
});
router.get("/project/:id", (req, res) => {
	return res.send(JSON.stringify(require("../data/project/projectId.json")));
});

/** Employee Api */
router.get("/employee/list", (req, res) => {
	return res.send(JSON.stringify(require("../data/employee/list.json")));
});

/** Company Api */
router.get("/company/list", (req, res) => {
	return res.send(JSON.stringify(require("../data/company/list.json")));
});

/** Employee Api */
router.get("/employee/list", (req, res) => {
	return res.send(JSON.stringify(require("../data/employee/list.json")));
});

/** Vacancy Api */
router.get("/vacancy/:id", (req, res) => {
	return res.send(JSON.stringify(require("../data/vacancy/vacancyId.json")));
});
router.get("/vacancy/list/demanded", (req, res) => {
	return res.send(JSON.stringify(require("../data/vacancy/demanded.json")));
});

/** Direction Api */
router.get("/direction/list", (req, res) => {
	return res.send(JSON.stringify(require("../data/direction/list.json")));
});
router.get("/direction/:id", (req, res) => {
	return res.send(JSON.stringify(require("../data/direction/directionId.json")));
});
router.get("/direction/list", (req, res) => {
	return res.send(JSON.stringify(require("../data/direction/list.json")));
});
router.get("/direction/:id", (req, res) => {
	return res.send(JSON.stringify(require("../data/direction/directionId.json")));
});
router.get("/direction/list/with-vacancies", (req, res) => {
	return res.send(JSON.stringify(require("../data/direction/withVacancies.json")));
});
router.get("/direction/list/demanded", (req, res) => {
	return res.send(JSON.stringify(require("../data/direction/demanded.json")));
});

/** Contacts Api */
router.get("/contact/page", (req, res) => {
	return res.send(JSON.stringify(require("../data/contact/contact.json")));
});
router.get("/contact/footer", (req, res) => {
	return res.send(JSON.stringify(require("../data/contact/footer.json")));
});

/** Special block Api */
router.get("/special-block/list", (req, res) => {
	return res.send(JSON.stringify(require("../data/special-block/projectTypes.json")));
});

/** Special block Api */
router.post("/feedback/send", (req, res) => {
	return res.send(JSON.stringify(require("../data/feedback/success.json")));
});

module.exports = router;