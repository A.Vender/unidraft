// Components
import Typography from '../Typography';

// Styles
import styles from './styles.module.scss';

const Textarea = ({ label, required, placeholder, textareaId, onChange, onBlur, inputId }) => (
	<div className={styles.formtextarea}>
		{label && (
			<div
				className={`
          ${styles.formtextarea_label}
          ${required && styles.__required}
        `}
			>
				<Typography.Text
					size="s"
					text={label}
				/>
			</div>
		)}
		<textarea
			id={textareaId}
			placeholder={placeholder}
			onChange={onChange}
			onBlur={(e) => onBlur(inputId, e.target.value)}
		/>
	</div>
);

export default Textarea;