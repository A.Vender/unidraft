import { useState, useEffect } from 'react';

// Styles
import styles from './styles.module.scss';

/**
 *
 * @param filtersList - Список фильтров в виде обьекта с id и названием фильтра
 * @param filtersType - Тип фильтрации: filter или anchors;
 * @param activeFilter - Текущий активный фильтр;
 * @param onClick - Callback по клику возвращает id выбранного фильтра;
 */
const Filters = ({
	filtersList,
	filtersType,
	activeFilter,
	onClick,
}) => {
	const [nodesItem, setNodesItem] = useState([]);

	useEffect(() => {
		if (filtersType !== 'anchors') return;
		const nodesIdList = filtersList.map(item => {
			return document.querySelector(`#block-${item.id}`);
		});
		setNodesItem(nodesIdList);
	}, []);

	const filtersCallback = (id, index) => {
		if (filtersType === 'filters') {
			onClick && onClick(id);
			document.body.scrollIntoView({ behavior: 'smooth', block: 'start' });
		} else if (filtersType === 'anchors') {
			const coords = nodesItem[index]?.getBoundingClientRect();

			onClick && onClick(id);
			if (window !== undefined) {
				window.scrollBy(0, (coords.top - 113) );
			}
		}
	};

	return (
		<div className={styles.filters}>
			<ul className={styles.filters_list}>
				{filtersList?.map((item, index) => (
					<li key={index}>
						<button
							onClick={() => filtersCallback(item.id, index)}
							className={`
								${styles.filters_button}
								${activeFilter === item.id ? styles.__active : ''}
							`}
						>
							{item.name}
						</button>
					</li>
				))}
			</ul>
		</div>
	);
}

export default Filters;