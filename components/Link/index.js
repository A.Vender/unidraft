// Components
import Typography from '../Typography';
import Icon from '../Icon';

// Styles
import styles from './styles.module.scss';

/**
 * Компонент отвечает за отображение ссылки;
 * @param url - Ссылка;
 * @param target - Как будет открываться ссылка; По default _self;
 * @param text - Текст ссылки;
 * @param type - Тип ссылки: underline, background. По default regular - без подчеркивания и фона;
 * @param size - Размер текста внутри ссылки;
 * @param width - Размер кнопки, если 100% кнопка становится flex и растягивается на весь экран;
 * @param iconName - Название иконки внутри ссылки;
 * @param color - Цвет ссылкиБ по умолчанию #000;
 * @param download - Поведение ссылки;
 */
const Link = ({ url, text, type, size, width, iconName, target, color, download }) => {
  const iconClassName = iconName ? styles.__icon : '';
  const typeClassName = type ? styles[`__${type}`] : '';
  const displayStyle = width === '100%' || iconName ? 'flex' : 'inline-block';

  return (
    <a
      download={download}
      target={target}
      href={url}
      style={{
        width,
        display: displayStyle,
      }}
      className={`
      ${styles.link}
      ${typeClassName}
      ${iconClassName}
    `}
    >
      {iconName && (
        <Icon iconName={iconName} />
      )}
      <Typography.Text
        text={text}
        size={size}
        color={color}
        tag="span"
      />
    </a>
  );
};

Link.defaultProps = {
  width: null,
  size: 'm',
  iconName: null,
  type: null,
  target: '_self',
  download: false
};

export default Link;
