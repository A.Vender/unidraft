import { useRef } from 'react';
import Image from 'next/image';

// Components
import Icon from '../Icon';

// Utils
import { STATIC_FILE_URL } from "../../utils/constants";

// Styles
import styles from './styles.module.scss';

/**
 * Превью проекта;
 * @param id - Идентификатор проекта;
 * @param url - Ссылка на проект;
 * @param projectTypeIds - массив с типами проекта;
 * @param projectTypeList - массив с общедоступными типами и названиями этих типов;
 * @param name - Название проекта;
 * @param projectInfo - Информация о проекте;
 * @param thumbnail - картинка превью проекта;
 * @param onClick - Callback по клику на кнопку посмотреть eye;
 * @param onTypeClick - Callback для получения id нажатого типа;
 * @param spaceArea - Один из параметров проекта отвечающий за прлощадь проекта;
 * @param language - Язык текста;
 */
const ProjectCard = ({
  id,
  url,
  language,
  projectTypeIds,
  projectTypeList,
  projectInfo,
  name,
  thumbnail,
  onClick,
  onTypeClick,
  spaceArea
}) => {
  const lazyRoot = useRef(null);

  return (
    <div
      className={styles.project}
      data-id={id}
      data-type="filters"
    >
      <a
        href={url}
        className={styles.project_link}
      />

      {thumbnail?.id && (
        <div
          ref={lazyRoot}
          style={{
            position: 'relative',
            width: `100%`,
            height: `${thumbnail.height}px`,
            backgroundColor: '#f2f2f2',
        }}>
          <Image
            alt={`image_${name}`}
            src={`/${thumbnail?.id}`}
            layout="fill"
            objectFit="cover"
            quality={100}
          />
        </div>
      )}

      <div className={styles.project_actions}>
        {onClick && (
          <div className={styles.project_actions__container}>
            <button
              className={styles.project_actions__button}
              onClick={onClick}
            >
              <Icon iconName="eye" />
            </button>
            <span>Смотреть</span>
          </div>
        )}

        <div className={styles.project_actions__container}>
          <a
            href={`/project/${id}`}
            className={styles.project_actions__button}
          >
            <Icon iconName="reply" />
          </a>
          <span>Перейти</span>
        </div>
      </div>

      <div className={styles.project_tags}>
        {projectTypeList?.map(item => (projectTypeIds.includes(item.id)) ? (
          <button
            key={item.id}
            onClick={() => onTypeClick(item.id)}
            className={`
              ${styles.project_tags__button}
              ${!onTypeClick && styles.__depricated}
            `}
          >
            {item.name}
          </button>
        ) : null)}
      </div>

      <div className={styles.project_info}>
        <div className={styles.project_info__left}>
          <div className={styles.project_info__unit}>Проект</div>
          <h3 className={styles.project_info__value}>{name}</h3>
        </div>
        <div className={styles.project_info__right}>
          <div className={styles.project_info__unit}>Общая площадь</div>
          <div className={styles.project_info__value}>{`${spaceArea} ${language === "Ru" ? "м" : "m"}²`}</div>
        </div>
      </div>
    </div>
  );
}

export default ProjectCard;
