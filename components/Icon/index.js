// Styles
import styles from './styles.module.scss';

/**
 * Компонент отвечает за отображение иконки;
 * @param iconName - название иконки по которому мы выбираем иконку из папки public/images/icons;
 * @param type - тип выбираемой иконки: svg, png; По default всегда svg;
 */
const Icon = ({ iconName, type }) => (
  <span className={styles.icon}>
    <img src={`/icons/${iconName}.${type}`} alt="" />
  </span>
);

Icon.defaultProps = {
  type: 'svg',
};

export default Icon;
