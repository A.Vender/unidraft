import styles from './styles.module.scss';

/**
 * Заголовок
 * @param tag - h1, h2, h3, h4, h5, h6; По default получает h1;
 * @param text - Текст заголовка;
 * @param weight - Насыщенность шрифта; По default получает 500 - Medium;
 * @param color - Цвет заголовка, по умолчанию он черный;
 */
const Title = ({ tag, text, weight, color }) => {
  const Comp = tag;

  return (
    <Comp
      className={`
        ${styles.title}
        ${styles[`title__${tag}`]}
        ${styles[`title__${color}`]}
      `}
      style={{ fontWeight: weight }}
    >
      {text}
    </Comp>
  )
};

Title.defaultProps = {
  tag: 'h1',
  weight: '500'
};


/**
 * Подзаголовок
 * @param text - Текст подзаголовка;
 * @param size - Размер текста: xl, l, m, s; По default получает l;
 * @param weight - Насыщенность шрифта; По default получает 500 - Medium;
 * @param color - Цвет заголовка, по умолчанию он черный;
 */
const SubTitle = ({ text, size, weight, color }) => (
  <div
    className={`
      ${styles.subtitle}
      ${styles[`subtitle__${size}`]}
      ${styles[`subtitle__${color}`]}
    `}
    style={{ fontWeight: weight }}
    dangerouslySetInnerHTML={{ __html: text }}
  />
);

SubTitle.defaultProps = {
  size: 'l',
  weight: '400'
};


/**
 *
 * @param tag - p, span; По default div;
 * @param text - Текст;
 * @param size - Размер текста: xl, l, m, s; По default получает l;
 * @param weight - Насыщенность шрифта; По default получает 400 - Regular;
 * @param color - Цвет заголовка, по умолчанию он черный;
 * @param children - Компонент;
 */
const Text = ({ tag, text, size, weight, color, children }) => {
  const Comp = tag;

  return (
    <Comp
      className={`
        ${styles.text}
        ${styles[`text__${size}`]}
        ${styles[`text__${color}`]}
      `}
      style={{ fontWeight: weight, color }}
    >
      <span dangerouslySetInnerHTML={{ __html: text }} />
      {children && children}
    </Comp>
  );
};

Text.defaultProps = {
  size: 'l',
  tag: 'div',
  weight: '400'
};


export default {
  Title,
  SubTitle,
  Text,
}
