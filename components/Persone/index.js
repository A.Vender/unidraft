// Components
import Typography from '../Typography';

// Utils
import { STATIC_FILE_URL } from "../../utils/constants";

// Styles
import styles from './styles.module.scss';

const Persone = ({ firstName, lastName, middleName, position, image }) => {
  const personeName = `${firstName || ''} ${middleName || ''} ${lastName || ''}`;
  return (
    <div className={styles.persone}>
      <div className={styles.persone_image}>
        <img src={`${STATIC_FILE_URL}/${image}`} alt={personeName} />
      </div>

      <div className={styles.persone_info}>
        <Typography.Title
          tag="h3"
          text={personeName}
        />
        <Typography.SubTitle
          tag="l"
          text={position}
          color="gray"
        />
      </div>
    </div>
  );
}

export default Persone;
