// Components
import Link from '../Link';
import Typography from '../Typography';

// Utils
import { STATIC_FILE_URL } from "../../utils/constants";

// Styles
import styles from './styles.module.scss';

/**
 * Контейнер с каралакс картинкой
 * @param image - Картинка в блоке с паралаксом
 * @param title - Заголовок блока
 * @param text - Текст блока
 * @param membrane - добавляет затемнение, для того, чтобы быыл виден текст на картинке;
 * @param paddingTop - Можно указывать высота блока padding в процентах
 * @param maxHeight - Максимальная высота блока
 * @param buttonText - Текст кнопки
 * @param buttonUrl - urk кнопки
 */
const ParallaxContainer = ({
  image,
  title,
  text,
  membrane,
  paddingTop,
  maxHeight,
  download,
  urls,
}) => (
  <div
    className={`
      ${styles.parallax}
      ${styles[membrane ? '__membrane' : '']}
    `}
    style={{
      paddingTop: `${paddingTop}%`,
      backgroundImage: `url(${STATIC_FILE_URL}/${image})`,
      backgroundColor: '#f2f2f2',
    }}
  >
    <div className={styles.parallax_info}>
      {title && (
        <Typography.Text
          size="xxl"
          text={title}
          color="#fff"
        />
      )}
      {text && (
        <div className={styles.parallax_info__text}>
          <Typography.Text
            size="l"
            text={text}
            color="#fff"
          />
        </div>
      )}

      {!!(urls?.length) && (
        <ul className={styles.parallax_buttons}>
          {urls?.map((item, index) => (
            <li key={index}>
              <Link
                download={download}
                type="background"
                url={item.url}
                text={item.label}
                color="#fff"
              />
            </li>
          ))}
        </ul>
      )}
    </div>
  </div>
);

ParallaxContainer.defaultProps = {
  paddingTop: 50
};

export default ParallaxContainer;
