import { useRef, useEffect } from 'react';

const TextAnimation = ({ text, id, delay, callback }) => {
	const textRef = useRef(null); // ref => { current: null }

	useEffect(() => {
		textRef.current.innerHTML = textRef.current.textContent
			.replace(/\S/g, "<span class='letter'>$&</span>");

		anime.timeline({ loop: false })
			.add({
				targets: `#anim_${id} .letter`,
				opacity: [0,1],
				easing: "easeInOutQuad",
				duration: 2250,
				delay: (el, i) => delay * (i+1)
			}).add({
				targets: textRef.current,
				opacity: 0,
				duration: 1000,
				easing: "easeOutExpo",
				delay: 1000,
				complete: callback && callback,
			});
	}, [])

	return (
		<span id={`anim_${id}`} ref={textRef}>
			{text}
		</span>
	);
}

TextAnimation.defaultProps = {
	id: "text",
	delay: 200,
}

export default TextAnimation;