import { useEffect } from 'react';
import { parseCookies, setCookie } from 'nookies';

// Components
import Icon from '../../../Icon';
import Typography from '../../../Typography';

// Utils
import {
  MENU
} from "../../../../utils/staticContent";

// Styles
import styles from './styles.module.scss';

const Menu = ({ onClick }) => {
  const language = parseCookies().language || "Ru";

  const setLanguageCookies = (langStr) => {
    if (language !== langStr) setCookie(null, "language" , langStr, {
      maxAge: 3600,
      path: "/"
    });

    if (window !== undefined) window.location.reload();
  };

  return (
    <div className={styles.menu}>
      <div className={styles.menu_close}>
        <button
          onClick={onClick}
          className={styles.menu_close__button}
        >
          <Icon iconName="close" />
        </button>
      </div>

      <ul className={styles.menu_language}>
        {[ 'Ru', 'Eng' ].map((item, index) => (
          <li
            key={index}
            className={styles.menu_language_item}
          >
            <button
              onClick={() => setLanguageCookies(item)}
              className={styles.menu_language__button}
            >
              {item}
              <Icon iconName={item} />
            </button>
          </li>
        ))}
      </ul>

      <ul className={styles.menu_list}>
        {MENU[language].map((item, index) => (
          <li id={`menu-${item.alias}`} key={index}>
            <a href={item.url}>{item.name}</a>
          </li>
        ))}
      </ul>
    </div>
  );
}

export default Menu;
