import { Fragment, useState } from 'react';

// Components
import Filters from '../Filters';
import Menu from './components/Menu';

// Styles
import styles from './styles.module.scss';

const Navigation = ({ filtersList, filtersType, filtersOnCLick, activeFilter }) => {
  const [isNavigationOpen, setNavigationOpen] = useState(false);

  return (
    <Fragment>
      <header
        className={`
          ${styles.navigation}
          ${!!(filtersList?.length) && styles.__filters}
        `}
      >
        <div className={styles.navigation_fixed}>
          <div className={styles.navigation_container}>
            <div className={styles.navigation_wrapper}>
              <div className={styles.navigation_logo}>
                <a href="/">
                  <img src="/images/unidraft-logo.svg" alt="unidraft_logo" />
                </a>
              </div>

              <button
                className={styles.navigation_burger}
                onClick={() => setNavigationOpen(!isNavigationOpen)}
              >
                <span />
                <span />
                <span />
              </button>
            </div>
          </div>

          {!!(filtersList?.length) && (
            <Filters
              activeFilter={activeFilter}
              onClick={filtersOnCLick}
              filtersList={filtersList}
              filtersType={filtersType}
            />
          )}
        </div>
      </header>

      {isNavigationOpen && (
        <Menu
          onClick={() => setNavigationOpen(!isNavigationOpen)}
        />
      )}
    </Fragment>
  );
};

export default Navigation;
