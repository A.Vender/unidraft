import { useState, Fragment } from 'react';

// Components
import Form from '../Form';
import Modal from '../Modal';
import Typography from '../Typography';
import Link from '../Link';

// Styles
import styles from './styles.module.scss';

/**
 * @param footerText - Текст над футером содержащий в себе кнопку которая отображется если прокинут buttonText
 * @param buttonText - Текст кнопки на которую завязоно ее отображение
 * @param address - Адрес
 * @param email - Электронный Адрес
 * @param phoneNumber - Телефонный номер
 * @param socialNetworks - Социальныя сеть
 * @param language - Язык текста;
 */
const Footer = ({ phoneNumber, footerText, address, email, socialNetworks, language }) => {
  const [isModalOpen, setModalOpen] = useState(false);

  return (
    <Fragment>
      <div className={styles.footer}>
        {footerText && (
          <div className={styles.footer_appeal}>
            <Typography.Text
              size="xxl"
              text={footerText.replace('[<p>|</p>]gi', '')}
            >
              <button
                onClick={() => setModalOpen(!isModalOpen)}
                className={styles.footer_appeal__button}
              >
                <Typography.Text
                  size="xxl"
                  tag="span"
                  color="red"
                  text={language === "Ru" ? "Написать нам" : "Write to us"}
                />
              </button>
            </Typography.Text>
          </div>
        )}

        <div className={styles.footer_info}>
          <ul className={styles.footer_info__list}>
            {[{
              "value": `mailto:${email}`,
              "text": email
            }, {
              "value": `tel:${phoneNumber}`,
              "text": phoneNumber
            }, {
              "value": socialNetworks && socialNetworks[0]?.url,
              "text": socialNetworks && socialNetworks[0]?.label
            }].map((item, index) => (
              <li key={index}>
                <Link
                  size="l"
                  color="black"
                  url={item.value}
                  text={item.text}
                />
              </li>
            ))}
          </ul>

          <div className={styles.footer_info__logo}>
            <a href="/">
              <img
                src="/images/unidraft-logo.svg"
                alt="unidraft_logo"
              />
            </a>
          </div>

          {address && (
            <div className={styles.footer_info__address}>
              <Typography.Text
                text={address}
                size="l"
                color="black"
              />
            </div>
          )}

          <div className={styles.footer_info__copyright}>
            <Typography.Text
              text="© 2017 Unidraft LLC | All Rights Reserved"
              size="l"
              color="black"
            />
          </div>
        </div>
        <div className={styles.footer_venderlab}>
          <div>{language === "Ru"
            ? "Дизайн и разработка"
            : "Design & Develop by"
          }</div>{" "}<a href="https://vdlb.ru/" target="_blank">Venderlab</a>
        </div>
      </div>

      {isModalOpen && (
        <Modal onClick={() => setModalOpen(!isModalOpen)}>
          <div className={styles.footer_form}>
            <Form
              isFile
              title={language === "Ru" ? "Написать нам" : "Write to us"}
            />
          </div>
        </Modal>
      )}
    </Fragment>
  );
};

export default Footer;
