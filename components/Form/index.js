import { useState } from 'react';
import { parseCookies } from 'nookies';

// Components
import Button from '../Button';
import Typography from '../Typography';
import Input from '../Input';
import Textarea from '../Textarea';
import Icon from '../Icon';
import Preloader from '../Preloader';

// Utils
import {
	SERVER_ENV_URL,
	EMAIL_REG_EXP,
	PHONE_NUMBER_REG_EXP,
} from '../../utils/constants';

// Styles
import styles from './styles.module.scss';

/**
 * Компонент формы
 * @param title - Загловок формы
 * @param isFile - Отвечает за отображение кнопки загрузки
 */
const Form = ({ title, isFile }) => {
	const [isFetchStart, setIsFetchStart] = useState(false);
	const [isFetchStatus, setIsFetchStatus] = useState(null);
	const [isDisactiveButton, setIsDisactiveButton] = useState(true);
	const [formFile, setFormFile] = useState(null);
	const [formMessage, setFormMessage] = useState(null);
	const [formErrors, setFormErrors] = useState({
		fullName: false,
		email: false,
		phoneNumber: false,
		message: false,
	});
	const [requiredFormFields, setRequiredFormFields] = useState({
		fullName: "",
		email: "",
		phoneNumber: "",
		message: ""
	});

	const language = parseCookies().language || "Ru";

	const sendFormData = async () => {
		setIsFetchStart(!isFetchStart);

		let formData = new FormData();
		formData.append('FullName', requiredFormFields?.fullName);
		formData.append('Email', requiredFormFields?.email);
		formData.append('PhoneNumber', requiredFormFields?.phoneNumber);
		formData.append('Message', requiredFormFields?.message);
		formFile?.attachments && formData.append('Attachments', formFile?.attachments);

		try {
			const req = await fetch(`${SERVER_ENV_URL}/api/feedback/send`, {
				method: 'POST',
				body: formData,
			});

			const reqStatus = await req.json();

			if (reqStatus.status === 200) {
				setIsFetchStatus("success");
				setRequiredFormFields({ fullName: "", email: "", phoneNumber: "" });
				setFormFile({});
				setFormMessage(null);
			}

			setIsFetchStart(!isFetchStart);
		} catch (err) {
			console.log('ERROR IN SEND EED BACK ... ', err);
			setIsFetchStatus("error");
			setIsFetchStart(!isFetchStart);
		}
	}

	// Функция отвечает за валидацию полей;
	const validateField = (name, value) => {
		let isError = null;
		switch(name) {
			case 'fullName':
				isError = value?.length !== 0 && value?.length < 3;
				break;
			case 'message':
				isError = value?.length !== 0 && value?.length < 1;
				break;
			case 'email':
				isError = value?.length !== 0 && !EMAIL_REG_EXP.test(value);
				break;
			case 'phoneNumber':
				let cleanedNumber = value?.replace(PHONE_NUMBER_REG_EXP, '');
				isError = cleanedNumber?.length < 11 && cleanedNumber?.length !== 1 && cleanedNumber !== "";
				break;
		}

		const errorValidationResult = { ...formErrors, [name]: isError };

		validateButton(errorValidationResult);
		setFormErrors(errorValidationResult);
	}

	const validateButton = (validErr) => {
		const isValidError = !validErr?.fullName
			&& !validErr?.email
			&& !validErr?.phoneNumber
			&& !validErr?.message;

		const isValidForm = requiredFormFields?.fullName !== ""
			&& requiredFormFields?.email !== ""
			&& requiredFormFields?.phoneNumber !== ""
			&& requiredFormFields?.message !== "";

		setIsDisactiveButton(!(isValidForm && isValidError));
	}

	// Функция добавляет value обязательным полям;
	const handleUserInput = (name, value) => setRequiredFormFields({
		...requiredFormFields,
		[name]: name === 'phoneNumber'
			? value?.replace(PHONE_NUMBER_REG_EXP, '') : value
	});

	// Функция добавялет файл и его название в state;
	const handleImageChange = (e) => {
		e.preventDefault();
		const file = e.target.files[0];
		const fileValue = e.target.value;

		setFormFile({ fileName: file?.name, attachments: [file] });
	}

	return (
		<div className={styles.form}>
			<Typography.Title
				tag="h2"
				text={title}
			/>

			<ul className={styles.form_list}>
				{[{
					required: true,
					inputId: "fullName",
					type: "text",
					label: language === "Ru" ? "Имя" : "Name",
					iconName: "user",
					isError: formErrors.fullName
				}, {
					required: true,
					inputId: "email",
					type: "email",
					label: "Email",
					iconName: "mailto",
					isError: formErrors.email
				}, {
					required: true,
					inputId: "phoneNumber",
					type: "phone",
					label: language === "Ru" ? "Телефон" : "Phone",
					iconName: "tel",
					isError: formErrors.phoneNumber
				}].map((item, index) => (
					<li key={index}>
						<Input
							{...item}
							language={language}
							onBlurInput={validateField}
							handleUserInput={handleUserInput}
						/>
					</li>
				))}
				<li key="textarea">
					<Textarea
						required
						inputId="message"
						label={language === "Ru" ? "Сообщение" : "Message"}
						onChange={(e) => handleUserInput("message", e.target.value)}
						onBlur={validateField}
					/>
				</li>
				{isFile && (
					<li key="file-input">
						<div className={styles.form_upload_file}>
							<Icon iconName="file-black" />
							<input
								className={styles.form_upload_file__input}
								type="file"
								onChange={handleImageChange}
							/>
							<span
								className={styles.form_upload_file__text}
							>{formFile?.fileName
								|| (language === "Ru" ? "Загрузить файл" : "Upload file")}
							</span>
						</div>
					</li>
				)}
			</ul>

			{isFetchStatus === 'success' && (
				<div className={styles.form_result}>
					<div className={styles.form_result_icon}>
						<Icon iconName="check-double-line" />
					</div>
				</div>
			)}

			{isFetchStatus !== 'success' && isFetchStart && (
				<div className={styles.form_preloader}>
					<Preloader size="s" />
				</div>
			)}

			{isFetchStatus !== 'success' && !isFetchStart && (
				<Button
					disabled={isDisactiveButton}
					width="100%"
					text={language === "Ru" ? "Отправить письмо" : "Send a letter"}
					iconName="mailto-white"
					onClick={sendFormData}
				/>
			)}
		</div>
	);
}
export default Form;