// Components
import Typography from '../Typography';

// Styles
import styles from './styles.module.scss';

/**
 * Базовый контейнер для всего сайта;
 * Отвечает за отступы от шапки до footer
 * @param children - Контент сайта
 */
const Main = ({ children }) => (
  <div className={styles.main}>
    {children}
  </div>
);

/**
 * Конейнер для блоков подстраивается под ширину.
 * Если передать null в maxWidth размер будет max-width: 100%;
 * @param children - Контент блока;
 * @param bgColor - Цвет бэкграунда;
 * @param maxWidth - Максимальный размер контента; По default 1440px;
 * @param horizontalPadding - Добавляет горизонтальные padding;
 * @param verticalPadding - Добавляет вертикальные padding;
 * @param paddingTop - Задает только padding вверху
 * @param paddingBottom - Задачет только padding внизу
 * @param title - Заголовок блока
 * @param subTitle - Подзаголовок блока
 * @param blockId - Идентификатор блока
 */
const Block = ({
  blockId,
  children,
  bgColor,
  maxWidth,
  horizontalPadding,
  verticalPadding,
  paddingTop,
  paddingBottom,
  title,
  subTitle,
}) => (
  <div
    id={`block-${blockId}`}
    className={`
      ${styles.block}
      ${horizontalPadding ? styles.horizontal_padding : ''}
      ${verticalPadding ? styles.vertical_padding : ''}
      ${paddingTop ? styles.padding_top : ''}
      ${paddingBottom ? styles.padding_bottom : ''}
    `}
    style={{ backgroundColor: bgColor }}
  >
    <div
      style={{ maxWidth: `${maxWidth}` }}
      className={styles.block_container}
    >
      {(title || subTitle) && (
        <div className={styles.block_heading}>
          {title && (
            <Typography.Title
              tag="h2"
              text={title}
            />
          )}
          {subTitle && (
            <Typography.SubTitle
              size="l"
              text={subTitle}
              color="gray"
            />
          )}
        </div>
      )}
      {children}
    </div>
  </div>
);

Block.defaultProps = {
  maxWidth: '1440px',
};

export default {
  Main,
  Block,
}
