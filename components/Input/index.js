import { useEffect, useState } from 'react';
import InputMask from 'react-input-mask';

// Components
import Icon from '../Icon';
import Typography  from '../Typography';

// Utils
import {
  FORM_ERROR_OBJECT,
} from '../../utils/staticContent';

// Styles
import styles from './styles.module.scss';

const Input = ({
  inputId,
  language,
  onBlurInput,
  handleUserInput,
  isError,
  type,
  value,
  defaultValue,
  placeholder,
  required,
  iconName,
  label
}) => {
  const [inputValue, setInputValue] = useState(value);
  const [isFocus, setIsFocus] = useState(false);

  useEffect(() => {
    const val = defaultValue || value;
    setInputValue(val);
  }, [inputValue]);

  const onInputFocus = () => setIsFocus(!isFocus);
  const onInputBlur = (e) => {
    onBlurInput(inputId, e.target.value);
    setIsFocus(!isFocus);
  };
  const onChange = (e) => {
    handleUserInput(inputId, e.target.value);
  }

  return (
    <div className={styles.forminput}>
      {label && (
        <div
          className={`
            ${styles.forminput_label}
            ${required && styles.__required}
          `}
        >
          <Typography.Text
            size="s"
            text={label}
          />
        </div>
      )}
      <div className={`
        ${styles.forminput_input}
        ${isFocus && styles.__focus}
      `}>
        <Icon
          iconName={iconName}
        />
        <InputMask
          type={type}
          id={inputId}
          placeholder={placeholder}
          mask={inputId === 'phoneNumber' ? "+7 (999) 999-99-99" : null}
          onChange={onChange}
          value={inputValue}
          onFocus={onInputFocus}
          onBlur={onInputBlur}
          disableunderline="true"
        />
        {isError && (
          <div
            className={styles.forminput_input__error}
          >{FORM_ERROR_OBJECT[language][inputId]}</div>
        )}
      </div>
    </div>
  );
}

Input.defaultProps = {
  type: "text"
}

export default Input;