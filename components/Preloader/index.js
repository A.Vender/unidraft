// Styles
import styles from './styles.module.scss';

const Preloader = ({ size, color }) => (
  <span className={`
    ${styles.preloader}
    ${styles[`__${size}`]}
    ${styles[`__${color}`]}
  `} />
);

Preloader.defaultProps = {
  size: "l",
  color: "black"
}

export default Preloader;
