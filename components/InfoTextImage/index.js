// Components
import Typography from '../Typography';
import ImagesSet from '../ImagesSet';
import Link from '../Link';

// Styles
import styles from './styles.module.scss';

/**
 * Компонент от вечает за отображение текста и сета из картинок в одном блоке
 * @param title - Заголовок блока
 * @param text - Текст блока
 * @param gallery - Картинки для сета из картинок
 * @param subTitle - Подзаголовок для блока, выводится под заголовком
 * @param reverse - Меняет расположение блока с текстом и сетом из картинок
 * @param attachments <Array> - id Ссылка для кнопки в блоке, name: Текст кнопки. Если ссылка не пришла, кнопка не отобразиться
 * @param buttonIcon - Иконка в кнопке
 * @param blockIndex - Порядковый индекс блока
 */
const InfoTextImage = ({
  blockIndex,
  title,
  text,
  gallery,
  attachments,
  subTitle,
  reverse,
  buttonIcon,
}) => (
  <div className={`
    ${styles.textbox}
    ${styles[`__${reverse ? 'reverse' : ''}`]}
    ${styles[`__${blockIndex === 1 ? 'center' : ''}`]}
    ${styles[`__${!gallery ? 'center' : ''}`]}
  `}>
    <div
      className={`
        ${styles.textbox_info}
        ${!!(gallery?.length) && styles.__withimage}
      `}
    >
      {title && (
        <Typography.Title
          tag="h2"
          text={title}
        />
      )}

      {subTitle && (
        <Typography.SubTitle
          size="xl"
          text={subTitle}
          color="gray"
        />
      )}

      {text && (
        <div className={styles.textbox_info__description}>
          <Typography.Text
            size="l"
            text={text}
          />
        </div>
      )}

      {attachments && (
        <div className={styles.textbox_info__link}>
          {attachments.map((item, index) => (
            <Link
              key={index}
              url={item.id}
              color="white"
              type="background"
              text={item.name}
              iconName={buttonIcon}
            />
          ))}
        </div>
      )}
    </div>

    {!!(gallery?.length) && (
      <div className={styles.textbox_info__image}>
        <ImagesSet images={gallery} />
      </div>
    )}
  </div>
);

export default InfoTextImage;
