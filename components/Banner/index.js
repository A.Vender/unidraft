// Components
import Typography from '../Typography';
import Link from '../Link';

// Utils
import { STATIC_FILE_URL } from "../../utils/constants";

// Styles
import styles from './styles.module.scss';

/**
 * Компонент отвечает за сквозной баннер на всем сайте;
 * @param title - Заголовок;
 * @param text - Текст;
 * @param imgId - Картинка используется для background;
 * @param list - Список с сылками (к примеру список вакансий);
 * @param urls - Список кнопок кнопки;
 * @param type - Тип банера для url;
 */
const Banner = ({ title, text, imgId, list, urls, type }) => (
  <div
    className={styles.banner}
    style={{
      backgroundImage: `url('${STATIC_FILE_URL}/${imgId}')`,
    }}
  >
    <div className={styles.banner_content}>
      {title && (
        <Typography.Title
          tag="h2"
          text={title}
        />
      )}

      {text && (
        <div className={styles.banner_text}>
          <Typography.SubTitle
            text={text}
            size="l"
            color="gray"
          />
        </div>
      )}

      {!!list?.length && (
        <ul className={styles.banner_list}>
          {list.map((item, index) => (
            <li key={index}>
              <Link
                size="l"
                url={`/${type}/${item.id}`}
                text={item.title}
              />
            </li>
          ))}
        </ul>
      )}

      {!!(urls?.length) && (
        <ul className={styles.banner_buttons}>
          {urls.map((item, index) => (
            <li key={index}>
              <Link
                color="white"
                text={item.label}
                url={item.url}
                type="background"
              />
            </li>
          ))}
        </ul>
      )}
    </div>
  </div>
);

export default Banner;
