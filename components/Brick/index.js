// Components
import Typography from '../Typography';
import Link from '../Link';

// Styles
import styles from './styles.module.scss';

/**
 *
 * @param title - Заголовок плитки
 * @param subTitle - Подзаголовок плитки
 * @param list - Список в плитке
 * @param listSubTitle - Подзаголовок для списка
 * @param onClick - callback для отображения кнопки
 * @param buttonText - Текст кнопки
 */
const Brick = ({
  title,
  list,
  subTitle,
  onClick,
  buttonText,
  listSubTitle
}) => (
  <div
    className={styles.brick}
    onClick={onClick && onClick}
  >
    {title && (
      <Typography.Title
        text={title}
        tag="h3"
      />
    )}

    {subTitle && (
      <div className={styles.brick_subtitle}>
        <Typography.SubTitle
          size="l"
          text={subTitle}
          color="gray"
        />
      </div>
    )}

    {!!list?.length && (
      <div className={styles.links}>
        {listSubTitle && (
          <div className={styles.links_subtitle}>
            <Typography.Text
              size="s"
              text={listSubTitle}
              color="gray"
            />
          </div>
        )}
        <ul className={styles.links_list}>
          {list?.map((item, index) => (
            <li key={index}>
              <Link
                size="m"
                url={`/vacancy/${item.id}`}
                text={item.title}
                type="underline"
              />
            </li>
          ))}
        </ul>
      </div>
    )}

    {onClick && (
      <button className={styles.brick_button}>
        {buttonText}
      </button>
    )}
  </div>
);

export default Brick;
