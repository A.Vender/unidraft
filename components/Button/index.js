// Components
import Icon from '../Icon';

// Styles
import styles from './styles.module.scss';

/**
 * Компонент отвечает за отображение кнопки;
 * @param onClick - Колбэк ссобытие;
 * @param text - Текст кнопки;
 * @param width - Размер кнопки. Если 100% растягивается на весь экран;
 * @param iconName - Название иконки добавляемой в кнопку;
 * @param disabled - блокировка кнопки;
 */
const Button = ({ onClick, text, width, iconName, disabled }) => (
  <button
    disabled={disabled}
    onClick={onClick}
    className={styles.button}
    style={{ width }}
  >
    {iconName && (
      <Icon iconName={iconName} />
    )}
    {text}
  </button>
);

Button.defaultProps = {
  width: null,
  disabled: false,
};

export default Button;
