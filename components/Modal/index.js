import { useEffect } from 'react';

// Components
import Icon from '../Icon';

// Styles
import styles from './styles.module.scss';

/**
 * Модальное окно на сайте
 * @param children - Компонент поркинутый из вне;
 * @param onClick - callback по клику на кнопку закрыть;
 */
const Modal = ({ children, onClick }) => {
  useEffect(() => {
    document.body.style.overflow = 'hidden';
  }, []);

  const closeModal = (e) => {
    if (e.target.id === "container" || e.target.id === "close_modal_button") {
      document.body.style.overflow = null;
      onClick && onClick();
    }
  };

  return (
    <div
      className={styles.modal}
      onClick={closeModal}
    >
      <button
        id="close_modal_button"
        className={styles.modal_button}
        onClick={closeModal}
      >
        <Icon iconName="close-white" />
      </button>

      <div id="container" className={styles.modal_container}>
        {children}
      </div>
    </div>
  );
};

export default Modal;
