// Styles
import styles from './styles.module.scss';

const FakeBlock = ({ size }) => (
  <div
    className={styles.fakeblock}
    style={{ paddingTop: size }}
  />
);

export default FakeBlock;
