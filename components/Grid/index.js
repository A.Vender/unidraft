import React from 'react';
import Masonry from 'react-masonry-component';

// Styles
import styles from './styles.module.scss';

/**
 * Предназначена для вывода списка в виде беспорядочных плиток;
 * @param children - список чаелдов;
 */
const Brick = ({ children }) => (
  <div className={styles.masonry}>
    <Masonry>
      {children.map((item, index) => (
        <div
          key={index}
          className={styles.masonry_item}
        >
          {item}
        </div>
      ))}
    </Masonry>
  </div>
);

/**
 * Предназначена для вывода списка в виде упорядочных плиток;
 * @param children - список чаелдов;
 */
const Simple = ({ children }) => (
  <div className={styles.simple}>
    {children}
  </div>
);

export default {
  Brick,
  Simple
};
