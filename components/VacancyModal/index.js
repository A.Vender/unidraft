// Components
import Link from '../Link'
import Typography from '../Typography';

// Styles
import styles from './styles.module.scss';

/**
 * НАполнение для модального окна вакансий и направлений
 * @param vacancyList - Список вакансий
 * @param vacancyTitle - Загловок списка вакансий
 * @param title - Заголовок
 * @param text - Текст
 */
const VacancyModal = ({ vacancyList, vacancyTitle, title, text }) => (
  <div className={styles.vacancy}>
    <div className={styles.vacancy_info}>
      <Typography.Title
        tag="h3"
        text={title}
      />
      <Typography.Text
        size="l"
        text={text}
      />
    </div>

    <div className={styles.vacancy_job}>
      {vacancyTitle && (
        <Typography.Title
          tag="h3"
          text={vacancyTitle}
        />
      )}

      {!!(vacancyList?.length) && (
        <ul>
          {vacancyList.map(item => (
            <li key={item.id}>
              <Link
                size="l"
                url={`/vacancy/${item.id}`}
                text={item.title}
                type="underline"
              />
            </li>
          ))}
        </ul>
      )}
    </div>
  </div>
);

export default VacancyModal;