// Styles
import styles from './styles.module.scss';

// Utils
import { STATIC_FILE_URL } from "../../utils/constants";

/**
 * Компонент применяется для колажа из трех картинок
 * @param images - список картинок
 */
const ImagesSet = ({ images }) => (
  <div className={styles.images}>
    {images.map((item, index) => (
      <div
        key={index}
        style={{
          backgroundImage: `url('${STATIC_FILE_URL}/${item.id}')`,
          backgroundColor: '#f2f2f2',
        }}
        className={styles.images_item}
      />
    ))}
  </div>
);

export default ImagesSet;
