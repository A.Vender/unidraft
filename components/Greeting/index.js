import { useEffect } from 'react';

// Components
import TextAnimation from '../TextAnimation';

// Styles
import styles from './styles.module.scss';

const Greeting = ({ text, callback }) => {
	useEffect(() => {
		if (document !== undefined) {
			document.cookie = "project=UNIDRAFT; max-age=3600; path=/";
		}
	}, []);

	const setCallback = () => {
		let timer = null;
		timer = setTimeout(() => {
			callback && callback();
			clearTimeout(timer);
		}, 200);
	};

	return (
		<div className={styles.greeting}>
			<div className={styles.greeting_wrapper}>
				<TextAnimation
					text={text}
					id="greeting"
					callback={setCallback}
				/>
			</div>
		</div>
	);
}

export default Greeting;