// Components
import Typography from '../../../Typography';

// Styles
import styles from './styles.module.scss';

const PopupHeading = ({ projectInfo, title }) => (
  <div className={styles.heading}>
    {title && (
      <Typography.Text
        text={title}
        size="xxl"
      />
    )}
    <ul className={styles.heading_info}>
      {projectInfo?.map((item, index) => (
        <li key={index}>
          <Typography.SubTitle
            text={item.name}
            color="gray"
            size="m"
          />
          <div className={styles.heading_value}>
            <Typography.Text
              text={`${item.value} ${item.unit ? item.unit : ''}`}
              size="xl"
            />
          </div>
        </li>
      ))}
    </ul>
  </div>
);

export default PopupHeading;
