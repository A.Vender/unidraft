import { Fragment } from 'react';

// Components
import Icon from '../../../Icon';

// Styles
import styles from './styles.module.scss';

const ArrowButtons = ({ onClick }) => (
  <Fragment>
    {['arrow-left', 'arrow-right'].map((item, index) => (
      <button
        key={index}
        onClick={() => onClick(item)}
        className={styles.button}
      >
        <Icon iconName={item} />
      </button>
    ))}
  </Fragment>
);

export default ArrowButtons;
