// Components
import Icon from '../../../Icon';
import Typography from '../../../Typography';

// Styles
import styles from './styles.module.scss';

const RedirectLink = ({ url }) => (
  <a
    href={url}
    className={styles.link}
  >
    <Typography.Text
      size="l"
      text="Перейти"
    />
    <Icon iconName="reply-gray" />
  </a>
);

export default RedirectLink;
