// Component
import ArrowButtons from './components/ArrowButtons';
import PopupHeading from './components/PopupHeading';
import RedirectLink from './components/RedirectLink';

// Utils
import { STATIC_FILE_URL } from "../../utils/constants";

// Styles
import styles from './styles.module.scss';

/**
 * Компонент отвечает за отображение проекта в модальном окне
 * @param id - Идентификатор проекта;
 * @param name - Название проекта;
 * @param location - Параметр проекта отвечающий за название локации;
 * @param spaceArea - Параметр проекта отвечающий за площадь проекта;
 * @param constructionYear - Параметр проекта отвечающий за год постройки;
 * @param mainImage - Картинка проекта;
 * @param callback - Callback который возврящает индекс по нажатию на стрелку
 * @param projectIndex - Текущий индекс
 * @param projectLength - Общее кол-во items
 * @param language - Язык контента
 */
const ModalProjectCard = ({
  id,
  name,
  language,
  location,
  spaceArea,
  constructionYear,
  mainImage,
  callback,
  projectIndex,
  projectLength
}) => {
  const setDataProject = (type) => {
    let currIndex = projectIndex;

    if (type === 'arrow-left') {
      currIndex = projectIndex === 0 ? (projectLength - 1) : (projectIndex - 1);
    } else if (type === 'arrow-right') {
      currIndex = projectIndex === (projectLength - 1) ? 0 : (projectIndex + 1);
    }

    callback(currIndex);
  };

  return (
    <div className={styles.project}>
      <div className={`
          ${styles.project_buttons}
          ${styles.__desktop}
        `}>
        <ArrowButtons onClick={setDataProject} />
      </div>

      <div className={styles.project_wrapper}>
        <PopupHeading
          title={name}
          projectInfo={[{
            type: "spaceArea",
            name: (language === "Ru" ? "Общая площадь" : "Total area"),
            value: spaceArea,
            unit: (language === "Ru" ? "м²" : "m²")
          }, {
            type: "constructionYear",
            name: (language === "Ru" ? "Год реализации" : "Year of implementation"),
            value: constructionYear,
            unit: null
          }, {
            type: "location",
            name: (language === "Ru" ? "Расположение" : "Location"),
            value: location,
            unit: null
          }]}
        />

        <div className={styles.project_image}>
          {mainImage?.id && (
            <img src={`${STATIC_FILE_URL}/${mainImage?.id}`} alt={name} />
          )}

          <div className={`
            ${styles.project_buttons}
            ${styles.__mobile}
          `}>
            <ArrowButtons onClick={setDataProject} />
          </div>
        </div>

        <div className={styles.project_link}>
          <RedirectLink url={`/project/${id}`} />
        </div>
      </div>
    </div>
  );
};

export default ModalProjectCard;
